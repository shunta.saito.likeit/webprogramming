package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import controller.util.PasswordEncorder;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // ログインしていなかったらログイン画面に遷移。
      HttpSession session = request.getSession();
      // sessionのデータはUserだからUserにキャストする。
      User session_user = (User) session.getAttribute("userInfo");
       if (session_user == null) {
       response.sendRedirect("LoginServlet");
       } else {
        // ユーザ一覧のサーブレットにリダイレクト
        String loginId = request.getParameter("loginId");
        String name = request.getParameter("name");
        String birth_date = request.getParameter("birth_date");

        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);

        // 成功したらユーザ登録のjspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
      }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      // リクエストパラメータの入力項目を取得。
      String loginId = request.getParameter("loginId");
      String name = request.getParameter("name");
      String birth_date = request.getParameter("birth_date");
      String password = request.getParameter("password");
      String password_confirm = request.getParameter("password-confirm");
      
      UserDao userDao = new UserDao();
      // 例外開始。入力したログインIDが既にDBに既にあったら
      User sloginId = userDao.findByLoginInfo(loginId, password);
      if (sloginId != null) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");

        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        request.setAttribute("password", password);
        request.setAttribute("password_confirm", password_confirm);

        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // パスワードとパスワード(確認)の値が一致していなかったら
      if (!password.equals(password_confirm)) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");

        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        request.setAttribute("password", password);
        request.setAttribute("password_confirm", password_confirm);
        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      if (loginId.equals("") || name.equals("") || birth_date.equals("")
          || password.equals("") || password_confirm.equals("")) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");
        // 入力した項目を画面に表示するために値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        request.setAttribute("password", password);
        request.setAttribute("password_confirm", password_confirm);
        // 登録jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // 暗号化
      String encodestr = PasswordEncorder.main(password);

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      userDao.userAdd(loginId, name, birth_date, encodestr);
      /** テーブルに該当のデータが登録できた場合 * */

      // ユーザ一覧のサーブレットにリダイレクト なんでリダイレクトなの？フォワードの方がパフォーマンスいいのに
      response.sendRedirect("UserListServlet");
    }
}
