package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import controller.util.PasswordEncorder;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    // 下のHttpServletRequest request,でリクエストを取得できる。セッションを取得する時はまた
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      // sessionのデータはUserだからUserにキャストする。
      User session_user = (User) session.getAttribute("userInfo");

      if (session_user == null) {
        response.sendRedirect("LoginServlet");
      } else {

        // ユーザー詳細機能と同じようにどのユーザーの削除ボタンを押したかわかるようにする
        String id = request.getParameter("id");
        // 確認用：idをコンソールに出力
        System.out.println(id);
        UserDao userDao = new UserDao();
        User userDteil = userDao.userDetail(id);
        String name = request.getParameter("name");
        String birth_date = request.getParameter("birth_date");
      //ここで設定したのをエラーの時に再表示されるようにする。
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        // ユーザ情報更新jspにフォワード
        request.setAttribute("userDteil", userDteil);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
      }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      String id = request.getParameter("id");
      // 確認用：idをコンソールに出力
      System.out.println(id);

      String name = request.getParameter("name");
      String birth_date = request.getParameter("birth_date");
      String password = request.getParameter("password");
      String password_confirm = request.getParameter("password-confirm");
      
      // 未実装？パス以外は選択したユーザーに紐ずくデータを「ユーザー情報テーブルから取得して初期値として設定する。多分できてる？

      // パス以外は選択したユーザーに紐ずくデータを「ユーザー情報テーブルから取得して初期値として設定する。」 多分さっきのfindbyがどうのの件はここでする？
      // 上の二つはふんわりとできた。細かい確認はしてない。

      // パスとパス（確認）が違っていたらエラー
      if (!password.equals(password_confirm)) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");

        // 入力した項目を画面に表示するために値をセット（パスとパス（確認）はセットせずに空欄にする）
        String Id = request.getParameter("id");
        // 確認用：idをコンソールに出力
        System.out.println(Id);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        UserDao userDao = new UserDao();
        User userDteil = userDao.userDetail(Id);
        // ユーザ情報更新jspにフォワード

        request.setAttribute("userDteil", userDteil);
        // ユーザ情報更新jspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        
      } else if (name.equals("") || birth_date.equals("")) {
        // パス以外に未入力が合ったらエラー
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された文字は正しくありません");

        // 入力した項目を画面に表示するために値をセット
        String Id = request.getParameter("id");

        // 確認用：idをコンソールに出力
        System.out.println(Id);
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        UserDao userDao = new UserDao();
        User userDteil = userDao.userDetail(Id);

        // ユーザ情報更新jspにフォワード
        request.setAttribute("userDteil", userDteil);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);

      } else if (password.equals("") && password.equals("")) {
        // パスとパス（確認）が入力されてなかったらパスは更新せずに更新する。そうじゃなかったら全更新
        request.setAttribute("name", name);
        request.setAttribute("birth_date", birth_date);
        UserDao userDao = new UserDao();
        userDao.Update(name, birth_date, id);
        HttpSession session = request.getSession();
        session.setAttribute("userUpdate", userDao);
        response.sendRedirect("UserListServlet");
      } else {
        String encodestr = PasswordEncorder.main(password);

        UserDao userDao = new UserDao();
        userDao.allUpdate(name, birth_date, encodestr, id);

        // セッションにユーザの情報をセット
        HttpSession session = request.getSession();
        session.setAttribute("userUpdate", userDao);
        response.sendRedirect("UserListServlet");
      }
      // ユーザ一覧のサーブレットにリダイレクト
	}
}
