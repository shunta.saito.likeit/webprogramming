CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

CREATE TABLE user(
	id SERIAL PRIMARY KEY AUTO_INCREMENT,
	login_id VARCHAR(255) UNIQUE NOT NULL,
	name VARCHAR(255) NOT NULL,
	birth_date DATE NOT NULL,
	password VARCHAR(255) NOT NULL,
	is_admin BOOLEAN NOT NULL DEFAULT 0,
	create_date DATETIME NOT NULL,
	update_date DATETIME NOT NULL
);
ユーザーを登録
INSERT INTO user (login_id, name,birth_date,password,is_admin,create_date,update_date) VALUES('admin', '管理者','2022-04-22','password','1',NOW(),NOW());
